

module "gke_auth" {
  source = "terraform-google-modules/kubernetes-engine/google//modules/auth"
  depends_on   = [module.gke]
  project_id   = var.project_id
  location     = module.gke.location
  cluster_name = module.gke.name
}

#In case we put in private the repo, we can uncomment this to upload the kubeconfig as  an artifact
# resource "local_file" "kubeconfig" {
#   content  = module.gke_auth.kubeconfig_raw
#   filename = "kubeconfig-${var.project_env}"
# }

module "gke" {
  source                 = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster"
  project_id             = var.project_id
  name                   = "${var.cluster_name}-${var.project_env}"
  regional               = true
  region                 = var.project_region
  network                = module.gcp-network.network_name
  subnetwork             = module.gcp-network.subnets_names[0]
  ip_range_pods          = var.ip_range_pods_name
  ip_range_services      = var.ip_range_services_name
  node_pools = [
    {
      name                      = "${var.cluster_name}-${var.node_pool_name}-${var.project_env}"
      machine_type              = var.k8s_nodes_type
      node_locations            = var.node_pool_locations
      min_count                 = var.k8s_nodes_min_count
      max_count                 = var.k8s_nodes_max_count
      disk_size_gb              = var.k8s_nodes_disk_size_gb
    },
  ]
}