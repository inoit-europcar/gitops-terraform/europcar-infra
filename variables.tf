#Project vars
variable "project_id" {
  type        = string
  description = "The project ID to host the cluster in"
}
# define Project region
variable "project_region" {
  type        = string
  description = "Project region"
}
# define GCP project env
variable "project_env" {
  type        = string
  description = "Project envirorment"
}

#Networking vars
variable "network" {
  description = "The VPC network created to host the cluster in"
  default     = "gke-network"
}
variable "subnetwork" {
  description = "The subnetwork created to host the cluster in"
  default     = "gke-subnet"
}
variable "ip_range_pods_name" {
  description = "The secondary ip range to use for pods"
  default     = "ip-range-pods"
}
variable "ip_range_services_name" {
  description = "The secondary ip range to use for services"
  default     = "ip-range-services"
}

#k8s vars
variable "cluster_name" {
  description = "The name for the GKE cluster"
  default     = "ie-cluster"
}
variable "node_pool_name" {
  description = "The name for the GKE node pools"
  default     = "node-pool"
}
variable "node_pool_locations" {
  type        = string
  description = "The locations for the node-pools"
}
# define node pool machine type
variable "k8s_nodes_type" {
  type        = string
  description = "node pool machine type"
}
variable "k8s_nodes_min_count" {
  type        = number
  description = "node pool minimum number"
}
variable "k8s_nodes_max_count" {
  type        = number
  description = "node pool maximum number"
}
variable "k8s_nodes_disk_size_gb" {
  type        = number
  description = "node pool disk size in gb"
}