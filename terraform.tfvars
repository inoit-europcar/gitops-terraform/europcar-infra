# GCP Settings
project_id = "storied-polymer-330916"
project_region  = "europe-west1"
project_env = "prod"


#Networking Vars

# k8s
k8s_nodes_type = "e2-medium"
node_pool_locations = "europe-west1-b,europe-west1-c,europe-west1-d"
k8s_nodes_min_count = 1
k8s_nodes_max_count = 1
k8s_nodes_disk_size_gb = 30