We can initialize the terraform by command if we set this backend on the provider. Usefull for CD on multiple envs

terraform init -backend-config="bucket=for-tech-test" -backend-config="prefix=prod"

terraform {
  backend "gcs" {
  }
}


Had to enable cloud resource management api on google cloud:
https://console.developers.google.com/apis/api/cloudresourcemanager.googleapis.com/overview?project=386409544685

Some sources:
https://dev.to/fabiomatavelli/setup-gitlab-ci-with-terraform-26l1