module "gcp-network" {
  source       = "terraform-google-modules/network/google"
  version      = "~> 2.5"
  project_id   = var.project_id
  network_name = "${var.network}-${var.project_env}"

  subnets = [
    {
      subnet_name   = "${var.subnetwork}-${var.project_env}"
      subnet_ip     = "10.10.0.0/16"
      subnet_region = var.project_region
    },
  ]

  secondary_ranges = {
    "${var.subnetwork}-${var.project_env}" = [
      {
        range_name    = var.ip_range_pods_name
        ip_cidr_range = "10.20.0.0/16"
      },
      {
        range_name    = var.ip_range_services_name
        ip_cidr_range = "10.30.0.0/16"
      },
    ]
  }
}
