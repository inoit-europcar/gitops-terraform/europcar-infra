terraform {
  required_version = ">= 0.12"
  backend "gcs" {
      credentials = "credentials/serviceaccount.json"
  }
}

provider "google" {
  project     = var.project_id
  region      = var.project_region
  credentials = "${file("credentials/serviceaccount.json")}"
}

